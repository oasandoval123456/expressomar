// primer archivo que se escribe código
const express = require('express');
const bodyParser = require('body-parser'); // importa la dependencia

let app = express(); // app objeto de tipo express
// en el material de la plataforma
methodOverride = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods','GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

module.exports = app;